export class Employee {

    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        if(value.length < 4) {
            return console.log('Слишком короткое имя')
        }
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        if (value < 18) {
            return console.log('Вы не достигли совершеннолетия!');
        } else if ( value > 60) {
            return console.log('Вам пора на пенсию!');
        }
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        if (value < 200) {
            this._salary = 200;
            return console.log('Минимальная зарплата 200$!');
        }

        this._salary = value;
    }

}

