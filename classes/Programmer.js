import { Employee } from "./Employee.js";

export class Programmer extends Employee {
    _salaryBonus = {
        low: 100,
        medium: 350,
        high: 800,
    }

    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang
    }

    set salary(value) {
        this._salary = value * 3;
    }

    showSalaryWithBonus() {
        const skills = this.lang.length;
        const { low, medium, high } = this._salaryBonus;

        if (skills <= 2) {
            this._salary = this._salary + low;
            return this._salaryBonus = low;
        } else if (skills > 2 && skills <= 4) {
            this._salary = this._salary + medium;
            return this._salaryBonus = medium;
        } else {
            this._salary = this._salary + high;
            return this._salaryBonus = high;
        }

    }

}