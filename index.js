/*
Теоретичне питання

1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
В прототипном наследовании у каждого объекта есть внутренняя ссылка на другой объект,
который называется его прототипом. Объекты заимствуют все свойства и методы от своих прототипов.

2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
В конструкторе ключевое слово super() используется как функция, вызывающая родительский конструктор.
Ключевое слово super также может быть использовано для вызова функций родительского объекта.
 */

import {Programmer} from "./classes/Programmer.js";

const programmer1 = new Programmer('Ilon', 21, 250, ['HTML', 'CSS']);
const programmer2 = new Programmer('David', 25, 600, ['HTML', 'CSS', 'JS', 'Git']);
const programmer3 = new Programmer('Simon', 32, 1500, ['HTML', 'CSS', 'JS', 'TypeScript', 'NodeJS', 'React', 'Git']);

programmer1.showSalaryWithBonus();
programmer2.showSalaryWithBonus();
programmer3.showSalaryWithBonus();

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);